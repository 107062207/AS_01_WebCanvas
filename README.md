# Software Studio 2020 Spring
## Assignment 01 Web Canvas

# 107062207 謝立倫
### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | N         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | N         |
| Un/Re-do button                                  | 10%       | N         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

排版簡單說明:
    底部棕色為body的顏色，canvas具有灰色邊框，為了美觀與右上的save鍵、左上reset鍵、以及下方toolbar區塊同色。右上upload鍵則較深以和save區分。其他功能皆位於下方toolbar。canvas底部為非純白淡色。


功能介紹:
1) Basic control tools:
   包含筆刷以及橡皮擦，可藉由共用的-鍵與+鍵調整粗細。-+鍵左方數字為粗細程度，初始值5，一次調整五，範圍5-40。筆刷亦可以調整顏色。點選erase鍵切換成橡皮擦，選取顏色切換成筆刷並更新顏色。
2) Cursor icon:
   筆刷與橡皮擦的游標符號不同，筆刷為crosshair圖示，橡皮擦則為move圖示，游標位於canvas範圍內時顯示。其餘為default形式。
3) Refresh button:
   位於左上角有一reset鍵，按下canvus清空，游標切換成筆刷。

4) Download:
   右上角具有save鍵，按下後canvas成為照片存檔於電腦，檔名genius.png

備註: 雖然有text輸入位置以及upload鍵，不過功能未能實踐，僅供觀賞。

### Gitlab page link

 "https://107062207.gitlab.io/AS_01_WebCanvas"



<style>
table th{
    width: 100%;
}
</style>