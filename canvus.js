//tool 0=pen, 1= erase, 2=line, 3=circle, 4= triangle, 5= square, 6 =text
//var tool = 0;


//eraser
var era = document.getElementById("eraser");
era.addEventListener('click', function(){     
    canvas.style.cursor= "move";    
    ctx.globalCompositeOperation="destination-out";
    pen();
});


//draw
var canvas = document.getElementById("myCanvas");

var ctx = canvas.getContext("2d");
var  drag= false;
var tk = 5;
ctx.lineWidth=10;

function  pen(e){
    if(drag == true ){

        ctx.fillStyle = co ;
        ctx.strokeStyle = co ;
        ctx.lineTo(e.clientX, e.clientY);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(e.clientX, e.clientY, tk, 0, Math.PI*2);
        ctx.fill();
        ctx.beginPath();
        ctx.moveTo(e.clientX, e.clientY); 
    }
}


canvas.addEventListener('mousemove', pen);

canvas.addEventListener('mouseup', function(){         

            drag = false;
            ctx.beginPath();
        });
canvas.addEventListener('mousedown', function(e){
        drag = true;
        pen();
});

//thickness
var settk = function(newtk){
    if(newtk<mintk)
        newtk = mintk;
    else if(newtk>maxtk)
        newtk = maxtk;
    tk = newtk;
    ctx.lineWidth = tk*2;

    tkSpan.innerHTML = tk;
}

var mintk = 5,
    maxtk = 50,
    defaulttk = 5,
    interval = 5,
    tkSpan = document.getElementById('tkval'),
    detk = document.getElementById('detk'),
    intk = document.getElementById('intk');

detk.addEventListener('click', function(){
    settk(tk-interval);
});

intk.addEventListener('click', function(){
    settk(tk+interval);
});

settk(defaulttk);


//color
var clp = document.getElementById("col");
var co;

clp.addEventListener("change", watchColorPicker);

function watchColorPicker(e) {
    co=clp.value;
    ctx.globalCompositeOperation="source-over";
    canvas.style.cursor= "crosshair";   
}

//reset
document.getElementById('rst').addEventListener('click', function() {
    ctx.clearRect(10, 10, 1050, 500);
    ctx.globalCompositeOperation="source-over"
    canvas.style.cursor= "crosshair";   
  }, false);

//upload

//redo

//undo

//shapes

//text
var words = document.getElementById("tx")
var w=words.value;

document.getElementById('enter').addEventListener('click', function() {

    ctx.font ="35px Times New Roman";
    ctx.fillText(w, 1500, 200);
  }, false);




